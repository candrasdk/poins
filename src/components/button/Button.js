import React from 'react';
import {
    TouchableOpacity, Text, View, ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import {
    COLOR_BLACK, COLOR_DISABLED, COLOR_TRANSPARENT_DARK, COLOR_WHITE, FONT_NUNITO_BOLD, FONT_NUNITO_EXTRA_BOLD, FONT_NUNITO_SEMI_BOLD,
} from '../../tools/constant';

const styles = {
    buttonLoginContainer: {
        backgroundColor: '#FF6600',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        height: 45,
        paddingHorizontal: 25,
    },
    disabled: {
        backgroundColor: COLOR_DISABLED,
    },
    textStyle: {
        color: COLOR_WHITE,
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        // fontFamily: 'Poppins'
    },
};

const enabledButton = (caption, containerStyle, textStyle, onPress) => (
    <TouchableOpacity style={[styles.buttonLoginContainer, containerStyle]} onPress={onPress}>
        <Text style={[styles.textStyle, textStyle]}>
            {caption}
        </Text>
    </TouchableOpacity>
);

const disabledButton = (caption, containerStyle, textStyle) => (
    <View style={[styles.buttonLoginContainer, styles.disabled, containerStyle]}>
        <Text style={[styles.textStyle, textStyle]}>
            {caption}
        </Text>
    </View>
);

const loadingButton = (containerStyle) => (
    <View style={[styles.buttonLoginContainer, containerStyle]}>
        <ActivityIndicator size="large" color={COLOR_TRANSPARENT_DARK} />
    </View>
);

const Button = ({
    disabled, loading, caption, containerStyle, textStyle, onPress,
}) => {
    if (loading) {
        return loadingButton(containerStyle);
    } if (disabled) {
        return disabledButton(caption, containerStyle, textStyle);
    }
    return enabledButton(caption, containerStyle, textStyle, onPress);
};

Button.propTypes = {
    caption: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
    onPress: PropTypes.func,
    containerStyle: PropTypes.object,
    textStyle: PropTypes.object,
};

Button.defaultProps = {
    disabled: false,
    loading: false,
    containerStyle: {},
    textStyle: {},
};

export default Button;
