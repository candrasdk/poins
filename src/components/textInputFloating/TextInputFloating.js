import React, { useEffect, useRef, useState } from 'react'
import {
  Text,
  TextInput,
  StyleSheet,
  View,
  Animated,
  Easing,
  TouchableWithoutFeedback,
} from 'react-native'
import { COLOR_BLACK, COLOR_DISABLED } from '../../tools/constant'



const TextInputFloating = (props) => {
  const {
    label, errorText, value, style, onBlur, onFocus,
    iconType, IconName, iconSize, iconColor, iconStyle,
    iconActive, onPressed, ...restOfProps
  } = props
  const [isFocused, setIsFocused] = useState(false)
  const inputRef = useRef(null)
  const focusAnim = useRef(new Animated.Value(0)).current

  useEffect(() => {
    Animated.timing(focusAnim, {
      toValue: isFocused || !!value ? 1 : 0,
      duration: 150,
      easing: Easing.bezier(0.4, 0, 0.2, 1),
      useNativeDriver: true,
    }).start()
  }, [focusAnim, isFocused, value])

  let color = isFocused ? '#23286B' : COLOR_DISABLED
  let borderColor = isFocused ? '#23286B' : COLOR_DISABLED
  if (errorText) {
    color = '#B00020'
    borderColor = '#B00020'
  }
  let Tag = iconType;
  return (
    <View style={style}>
      <TextInput
        style={[
          styles.input(iconActive, isFocused),
          {
            borderColor: borderColor,
          },
        ]}
        ref={inputRef}
        {...restOfProps}
        value={value}
        onBlur={(event) => {
          setIsFocused(false)
          onBlur?.(event)
        }}
        onFocus={(event) => {
          setIsFocused(true)
          onFocus?.(event)
        }}
      />
      <TouchableWithoutFeedback onPress={() => inputRef.current?.focus()}>
        <Animated.View
          style={[
            styles.labelContainer(isFocused, value, iconActive),
            {
              transform: [
                {
                  scale: focusAnim.interpolate({
                    inputRange: [0.3, 1.8],
                    outputRange: [1, 0.6],
                  }),
                },
                {
                  translateY: focusAnim.interpolate({
                    inputRange: [0, 1],
                    outputRange: [24, -12],
                  }),
                },
                {
                  translateX: focusAnim.interpolate({
                    inputRange: [0, 1],
                    outputRange: [16, 0],
                  }),
                },
              ],
            },
          ]}
        >
          {/* <View style={styles.tes}/> */}
          <Text
            style={[
              styles.backLabel(value, isFocused, iconActive),
            ]}
          >
            {label}
          </Text>
          <Text
            style={[
              styles.label,
              {
                color,
              },
            ]}
          >
            {label}
            {errorText ? '*' : ''}
          </Text>

        </Animated.View>
      </TouchableWithoutFeedback>
      {iconType && iconActive ?
        <Tag
          name={IconName}
          size={iconSize}
          color={isFocused ? iconColor : COLOR_DISABLED}
          style={{
            backgroundColor: 'white',
            alignSelf: 'center',
            position: 'absolute',
            right: 12,
            top: 15
          }}
          onPress={onPressed}
        /> : null
      }
      {!!errorText && <Text style={styles.error}>{errorText}</Text>}
    </View>
  )
}

const styles = StyleSheet.create({
  input: (iconActive, isFocused) => ({
    padding: 16,
    borderWidth: 1,
    borderRadius: 4,
    fontFamily: 'Avenir-Medium',
    fontSize: isFocused ? 16 : 14,
    paddingRight: iconActive ? 50 : 16,
  }),
  labelContainer: (isFocused, value, iconActive) => ({
    position: 'absolute',
    paddingHorizontal: 8,
    paddingLeft: iconActive ? isFocused ? 0 : value ? 0 : 10 : isFocused ? 0 : value ? 0 : 10,
    top: isFocused || value ? 0 : -10,
    width: '50%'
  }),
  backLabel: (value, isFocused) => ({
    position: 'absolute',
    backgroundColor: value ? 'white' : isFocused ? 'white' : null,
    height: 20,
    color: 'white',
    paddingHorizontal: 10,
    left: -8
  }),
  label: {
    fontFamily: 'Avenir-Heavy',
    color: COLOR_DISABLED,
    fontSize: 15,
    zIndex: 2,
  },
  error: {
    marginTop: 4,
    marginLeft: 12,
    fontSize: 12,
    color: '#B00020',
    fontFamily: 'Avenir-Medium',
  },
})

export default TextInputFloating