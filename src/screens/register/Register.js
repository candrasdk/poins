import React, { useState } from 'react'
import { StyleSheet, Text, Pressable, View } from 'react-native'
import { TextInputFloating, Button, KeyboardView } from "../../components";
import NavigationService from "../../tools/navigationService";
import { NAV_NAME_LOGIN, NAV_NAME_OTP_REGISTER } from '../../tools/constant';
import { android, iconTools } from "../../tools/helper";
import { SafeAreaProvider } from 'react-native-safe-area-context';

const Register = () => {
  const [email, setEmail] = useState('')
  const [phoneNumber, setPhoneNumber] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [iconPasssword, setIconPassword] = useState('eye')
  const [iconConfirmPasssword, setIconConfirmPassword] = useState('eye')
  const [showPassword, setShowPassword] = useState(true)
  const [showConfirmPassword, setShowConfirmPassword] = useState(true)

  const handlerVisiblePassword = () => {
    setShowPassword(!showPassword)
    if (!showPassword) return setIconPassword('eye')
    if (showPassword) return setIconPassword('eye-off')
  }
  const handlerVisibleConfirmPassword = () => {
    setShowConfirmPassword(!showConfirmPassword)
    if (!showConfirmPassword) return setIconConfirmPassword('eye')
    if (showConfirmPassword) return setIconConfirmPassword('eye-off')
  }

  return (
    <KeyboardView contentContainerStyle={{ flex: android ? null : 1 }}>
      <SafeAreaProvider style={{ backgroundColor: '#23286B', flex: 1, }}>
        <Text style={styles.title}>POINS</Text>
        <View style={styles.content}>
          <View>
            <TextInputFloating
              style={[{ marginBottom: 30 }]}
              iconType={iconTools.MaterialCommunityIcons}
              IconName={'email'}
              iconSize={24}
              maxLength={40}
              iconColor={'#23286B'}
              iconActive={true}
              value={email}
              label="Email"
              onChangeText={(text) => setEmail(text)}
            />
            <TextInputFloating
              style={[{ marginBottom: 30 }]}
              iconType={iconTools.MaterialCommunityIcons}
              IconName={'phone'}
              iconSize={24}
              keyboardType="number-pad"
              iconColor={'#23286B'}
              iconActive={true}
              maxLength={15}
              value={phoneNumber}
              label="Phone Number"
              onChangeText={(text) => {
                if (isNaN(Number(text))) {
                  // do nothing when a non digit is pressed
                  return;
                }
                setPhoneNumber(text)
              }}
            />
            <TextInputFloating
              secureTextEntry={showPassword}
              style={[{ marginBottom: 30 }]}
              iconType={iconTools.MaterialCommunityIcons}
              IconName={iconPasssword}
              iconSize={24}
              iconColor={'#23286B'}
              iconActive={true}
              value={password}
              label="Password"
              onPressed={() => handlerVisiblePassword()}
              onChangeText={(text) => setPassword(text)}
            />
            <TextInputFloating
              secureTextEntry={showConfirmPassword}
              style={[{ marginBottom: android ? 80 : 90 }]}
              iconType={iconTools.MaterialCommunityIcons}
              IconName={iconConfirmPasssword}
              iconSize={24}
              iconColor={'#23286B'}
              iconActive={true}
              value={confirmPassword}
              label="Confirm Password"
              onPressed={() => handlerVisibleConfirmPassword()}
              onChangeText={(text) => setConfirmPassword(text)}
            />
          </View>
          <Button caption='Continue' onPress={() => NavigationService.navigate(NAV_NAME_OTP_REGISTER)} />
          <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 20, marginBottom: 50 }}>
            <Text style={{ fontSize: 12, color:'#000' }} >Already have an account? </Text>
            <Pressable
              style={{}}
              onPress={() => NavigationService.navigate(NAV_NAME_LOGIN)}
            >
              <Text style={{ color: '#F9603B', fontWeight: '500' }}>Login</Text>
            </Pressable>
          </View>

        </View>
      </SafeAreaProvider>
    </KeyboardView>
  )
}

const styles = StyleSheet.create({
  content: {
    height: '100%',
    position: 'relative',
    backgroundColor: 'white',
    paddingHorizontal: 20,
    borderTopStartRadius: 30,
    borderTopEndRadius: 30,
    paddingTop: 60,
  },
  title: {
    color: 'white',
    fontSize: 36,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingVertical: '20%'
  },
})

export default Register