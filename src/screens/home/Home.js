import React from 'react'
import { StyleSheet, Text, TouchableOpacity, SafeAreaView, View } from 'react-native'
import { NAV_NAME_BOOKING, NAV_NAME_LOGIN, } from '../../tools/constant';
import NavigationService from "../../tools/navigationService";

const Home = () => {
    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: 'white',
                justifyContent: 'center',
            }}>
            {/* <View style={{ justifyContent: 'center', alignItems: 'center', marginHorizontal: 30 }}> */}
            <View style={{ flexDirection: 'column', width: '100%', justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => NavigationService.navigate(NAV_NAME_LOGIN)}
                    style={{
                        width: '80%',
                        height: 50,
                        backgroundColor: '#FF6600',
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 5,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 50,
                        marginVertical: 10
                    }}
                >
                    <Text style={{ fontSize: 25, color: '#000', fontWeight: '600' }}>
                        Go To Login
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => NavigationService.navigate(NAV_NAME_BOOKING)}
                    style={{
                        width: '80%',
                        height: 50,
                        backgroundColor: '#FF6600',
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 5,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 50,
                        marginVertical: 10
                    }}
                >
                    <Text style={{ fontSize: 25, color: '#000', fontWeight: '600' }}>
                        Go To Booking Slot
                    </Text>
                </TouchableOpacity>
            </View>
            {/* </View> */}
        </SafeAreaView>
    )
}

export default Home

const styles = StyleSheet.create({})