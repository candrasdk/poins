import React, { useEffect, useRef, useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, SafeAreaView, FlatList, View, Animated, Modal, Alert, Image } from 'react-native'
import { COLOR_BLACK, COLOR_WHITE, NAV_NAME_LOGIN, NAV_NAME_REGISTER } from '../../tools/constant';
import NavigationService from "../../tools/navigationService";
import { android, iconTools, ios } from '../../tools/helper';
import { Button } from '../../components';

const Booking = () => {
    const [chooseDay, setChooseDay] = useState(null)
    const [showModal, setShowModal] = useState(false)
    const scaleValue = useRef(new Animated.Value(0)).current;

    const stringMonth = [
        "Januari",
        "Februari",
        "Maret",
        "April",
        "May",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
    ];

    const arrHours = Array.from(Array(24), (_, i) => ({ hour: i, status: 'Available', isChecked: false }))
    arrHours[2].status = 'Booked'
    arrHours[4].status = 'Booked'
    arrHours[8].status = 'Booked'
    arrHours[9].status = 'Booked'
    arrHours[14].status = 'Booked'
    arrHours[15].status = 'Booked'
    arrHours[16].status = 'Booked'
    const [hours, setHours] = useState(arrHours)

    const enumerateDaysBetweenDates = (startDate, endDate) => {
        let now = startDate,
            dates = [];
        while (now <= endDate) {

            let month = stringMonth[now.getMonth()]
            let day = now.getUTCDate();
            let year = now.getUTCFullYear();
            dates.push({ day: day, month: month, year: year, status: 'avalaible' });

            now.setDate(now.getDate() + 1);
        }
        dates[5].status = 'booked'
        dates[12].status = 'booked'
        return dates;
    };

    let from = new Date();
    let to = new Date();
    to.setDate(to.getDate() + 14);

    let results = enumerateDaysBetweenDates(from, to);

    // handler checkbox
    const [checkedState, setCheckedState] = useState(
        new Array(hours.length).fill(false)
    );
    const handleOnChange = (data, position) => {
        console.log('FASFASF', arrHours.length, data.length);

        const updatedCheckedState = checkedState.map((item, index) =>
            index === position ? !item : item
        );
        setCheckedState(updatedCheckedState);
    };


    // handler MODAL

    const [filterValue, setFilterValue] = useState(null)
    const toggleModal = () => {
        setShowModal(true)
        Animated.spring(scaleValue, {
            toValue: 1,
            duration: 300,
            useNativeDriver: true,
        }).start();
    };


    const handlerBack = () => {
        setTimeout(() => setShowModal(false), 200)
        Animated.timing(scaleValue, {
            toValue: 0,
            duration: 300,
            useNativeDriver: true,
        }).start();
        // NavigationService.back()
    }

    const optionUpdate1 = (number) => {
        setCheckedState(new Array(hours.length).fill(false))
        setFilterValue(number)
        temp = []
        arrHours.map((item, index) => {
            if (item.hour < 12) {
                return temp.push(item)
            }
        })
        setHours(temp)
        setTimeout(() => setShowModal(false), 200)
        Animated.timing(scaleValue, {
            toValue: 0,
            duration: 300,
            useNativeDriver: true,
        }).start();
    }

    const optionUpdate2 = (number) => {
        setCheckedState(new Array(hours.length).fill(false))
        setFilterValue(number)
        temp = []
        arrHours.map((item, index) => {
            if (item.hour > 11) {
                return temp.push(item)
            }
        })
        setHours(temp)
        setTimeout(() => setShowModal(false), 200)
        Animated.timing(scaleValue, {
            toValue: 0,
            duration: 300,
            useNativeDriver: true,
        }).start();
    }


    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: 'white',
                paddingTop: android ? 25 : 0,
            }}
        >
            <View style={{ justifyContent: 'center', alignItems: 'center', marginHorizontal: ios ? 25 : 30 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%', marginBottom: 40 }}>
                    <TouchableOpacity
                        onPress={() => NavigationService.back()}
                        style={{
                            width: 50,
                            height: 50,
                            backgroundColor: 'white',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.25,
                            shadowRadius: 3.84,
                            elevation: 5,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 50,
                            marginRight: '30%',
                        }}
                    >
                        <iconTools.MaterialCommunityIcons name='arrow-left' size={25} color={'#23286B'} />
                    </TouchableOpacity>
                    <Text style={{ fontSize: 25, color: '#23286B', fontWeight: '600' }}>{
                        stringMonth[from.getMonth()]}
                    </Text>
                </View>
                <Text style={{ alignSelf: 'flex-start', fontSize: 20, fontWeight: '600', marginBottom: 10, color: '#23286B' }}>
                    Book A Slot
                </Text>
                <FlatList
                    data={results}
                    numColumns={5}
                    scrollEnabled={false}
                    keyExtractor={(item) => item.day}
                    renderItem={({ item }) => {
                        return (
                            <TouchableOpacity
                                onPress={() => item.status === 'avalaible' ? setChooseDay(item.day) : Alert.alert('Available schedule is no longer available on this date')}
                                style={{
                                    backgroundColor: item.status === 'avalaible' ? item.day === chooseDay ? '#FF6600' : '#23286B' : 'red',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    margin: 8,
                                    shadowColor: "#000",
                                    shadowOffset: {
                                        width: 0,
                                        height: 2,
                                    },
                                    shadowOpacity: 0.25,
                                    shadowRadius: 3.84,
                                    elevation: 5,
                                    width: 50,
                                    height: 45,
                                    borderRadius: 5
                                }}>
                                <Text style={{ fontSize: 15, fontWeight: '500', color: 'white' }}>{item.day}</Text>
                            </TouchableOpacity>
                        )
                    }}
                    style={{ height: 250 }}
                />
                <View style={{ width: '100%', height: 430 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
                        <Text style={{ fontSize: 20, fontWeight: '600', color: '#23286B' }}>
                            Time Scheduling
                        </Text>
                        <TouchableOpacity
                            style={{
                                backgroundColor: 'white',
                                alignItems: 'center',
                                justifyContent: 'center',
                                // margin: 10,
                                height: 45,
                                borderRadius: 5,
                                flexDirection: 'row',
                            }}
                            onPress={() => toggleModal()}>
                            <Text style={{ fontSize: 15, marginRight: 5, fontWeight: '500', opacity: .5, color: '#23286B' }}>
                                {!filterValue ? 'select range' : filterValue === 12 ? `00:00 - ${filterValue}:00` : `00:00 - ${filterValue}:00`}
                            </Text>
                            <iconTools.MaterialCommunityIcons name='filter' size={20} color={'#23286B'} />
                        </TouchableOpacity>
                    </View>
                    {chooseDay ?
                        <FlatList
                            contentContainerStyle={{ justifyContent: 'center' }}
                            showsVerticalScrollIndicator={false}
                            style={{ width: '100%' }}
                            data={hours}
                            keyExtractor={(_, item) => item}
                            renderItem={({ item, index }) => {
                                let value = item?.hour === 23 ? item?.hour - 23 : item?.hour + 1
                                return (
                                    <TouchableOpacity
                                        onPress={() => item?.status === 'Available' ? handleOnChange(hours, index) : Alert.alert('Already booked PT XYZ')}
                                        style={{
                                            alignItems: 'center',
                                            justifyContent: 'space-between',
                                            flexDirection: 'row',
                                            borderBottomWidth: 1,
                                            borderBottomColor: '#23286B',
                                            margin: 10,
                                            height: 50,
                                            borderRadius: 5,
                                        }}>
                                        <Text style={{ fontSize: 15, fontWeight: '500', color: '#23286B' }}>
                                            {`${item?.hour < 10 ? `0${item?.hour}` : item?.hour}:00 - ${value < 10 ? `0${value}` : value}:00`}
                                        </Text>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Text
                                                style={{
                                                    color: item?.status === 'Available' ? 'green' : 'red',
                                                    marginRight: 10,
                                                    borderWidth: 1,
                                                    borderColor: item?.status === 'Available' ? 'green' : 'red',
                                                    width: 70,
                                                    textAlign: 'center'
                                                }}>
                                                {item?.status}
                                            </Text>
                                            <View>
                                                <iconTools.MaterialCommunityIcons
                                                    name={!checkedState[index] ? 'checkbox-blank-circle-outline' : 'checkbox-marked-circle-outline'}
                                                    size={25}
                                                    color={'#23286B'}
                                                />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }}
                        /> :
                        <View style={{ alignItems: 'center', marginTop: 30 }}>
                            <Image source={require('../../assets/not-found.png')} style={{ height: 200, width: 300 }} />
                            <Text style={{ fontSize: 30, fontWeight: '500', color: '#CCCCCC' }}>Selet your date</Text>
                        </View>
                    }

                </View>
            </View>

            <Modal transparent visible={showModal} >
                <View style={[StyleSheet.absoluteFillObject, styles.modalBackGround]}>
                    <TouchableOpacity
                        onPress={() => handlerBack()}
                        style={{ height: '100%', width: '100%', position: 'absolute' }}
                    />
                    <Animated.View
                        style={[styles.modalContainer, { transform: [{ scale: scaleValue }] }]}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ marginVertical: 20, fontSize: 18, fontWeight: '600', textAlign: 'center', color: '#23286B' }}>
                                Choose your range
                            </Text>
                            <View style={styles.buttonContainer}>
                                <Button
                                    caption={'00:00 - 12:00 AM'}
                                    onPress={() => optionUpdate1(12)}
                                    containerStyle={{
                                        ...styles.buttonStyle,
                                        backgroundColor: '#23286B',
                                    }}
                                    textStyle={{ color: COLOR_WHITE }}
                                />
                                <Button
                                    caption={'12:00 - 00:00 PM'}
                                    onPress={() => optionUpdate2(23)}
                                    containerStyle={{
                                        ...styles.buttonStyle,
                                        backgroundColor: '#23286B',
                                    }}
                                    textStyle={{ color: COLOR_WHITE }}
                                />
                            </View>
                        </View>

                    </Animated.View>
                </View>
            </Modal>
        </SafeAreaView>
    )
}

export default Booking

const styles = StyleSheet.create({
    modalBackGround: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageContainer: {
        width: 200,
        height: 30,
    },
    modalContainer: {
        width: '80%',
        backgroundColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 20,
        elevation: 20,
        maxHeight: '80%',
    },
    buttonContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        paddingVertical: 15,
        bottom: ios ? 0 : 0,
        width: '100%',
    },
    buttonStyle: {
        width: '80%',
        borderRadius: 60,
        margin: 10,
    },
})