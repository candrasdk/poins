import React, { useRef, useState } from 'react'
import { StyleSheet, Text, View, TextInput, Keyboard, Image, StatusBar } from 'react-native'
import { BaseScreen, Button, PinInput, KeyboardView } from "../../components";
import { COLOR_BLACK, COLOR_DISABLED, COLOR_TRANSPARENT_DARK, NAV_NAME_HOME, NAV_NAME_LOGIN } from '../../tools/constant';
import { android } from '../../tools/helper';
import NavigationService from '../../tools/navigationService';


const OtpRegister = () => {
  const [otpArray, setOtpArray] = useState(['', '', '', '']);

  const firstTextInputRef = useRef(null);
  const secondTextInputRef = useRef(null);
  const thirdTextInputRef = useRef(null);
  const fourthTextInputRef = useRef(null);

  const refCallback = textInputRef => node => {
    textInputRef.current = node;
  };
  const onOtpChange = index => {
    return value => {
      if (isNaN(Number(value))) {
        // do nothing when a non digit is pressed
        return;
      }
      const otpArrayCopy = otpArray.concat();
      otpArrayCopy[index] = value;
      setOtpArray(otpArrayCopy);

      // auto focus to next InputText if value is not blank
      if (value !== '') {
        if (index === 0) {
          secondTextInputRef.current.focus();
        } else if (index === 1) {
          thirdTextInputRef.current.focus();
        } else if (index === 2) {
          fourthTextInputRef.current.focus();
        } else if (index === 3) {
          Keyboard.dismiss()
        }
      }
    };
  };
  const onOtpKeyPress = index => {
    return ({ nativeEvent: { key: value } }) => {
      // auto focus to previous InputText if value is blank and existing value is also blank
      if (value === 'Backspace' && otpArray[index] === '') {
        if (index === 1) {
          firstTextInputRef.current.focus();
        } else if (index === 2) {
          secondTextInputRef.current.focus();
        } else if (index === 3) {
          thirdTextInputRef.current.focus();
        }
        // if (android && index > 0) {
        //   const otpArrayCopy = otpArray.concat();
        //   otpArrayCopy[index - 1] = '';
        //   setOtpArray(otpArrayCopy);
        // }
      }
    };
  };

  return (
    <BaseScreen>
     <StatusBar barStyle='dark-content' />
      <KeyboardView contentContainerStyle={{ flex: 1 }}>
        <View style={{ justifyContent: 'space-evenly', alignItems: 'center', flex: 1, }}>
          <View style={{ alignItems: 'center', }}>
            <Image source={{uri:'https://cdn-icons-png.flaticon.com/512/7603/7603257.png'}} style={{ height: 200, width: 200 }} />
            <Text style={{ fontSize: 25, fontWeight: '700', letterSpacing: 2, marginBottom: 5,color:COLOR_BLACK  }}>CREATE YOUR PIN</Text>
            <Text style={{ fontSize: 14, fontWeight: '400', letterSpacing: 1, color:COLOR_DISABLED  }}>This keep your account secure</Text>
          </View>
          <View style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
            {[
              firstTextInputRef,
              secondTextInputRef,
              thirdTextInputRef,
              fourthTextInputRef,
            ].map((textInputRef, index) => (
              <PinInput
                containerStyle={{ alignItems: 'center', marginBottom: 50 }}
                value={otpArray[index]}
                onKeyPress={onOtpKeyPress(index)}
                onChangeText={onOtpChange(index)}
                keyboardType={'numeric'}
                maxLength={1}
                style={styles.customInputOTP}
                autoFocus={index === 0 ? true : undefined}
                refCallback={refCallback(textInputRef)}
                key={index}

              />
            ))}
          </View>
          <Button
            containerStyle={{ borderRadius: 10, width: '80%', }}
            caption='CONFIRM'
            onPress={() => NavigationService.replace(NAV_NAME_HOME)}
          />
        </View>
      </KeyboardView>
    </BaseScreen>
  )
}

export default OtpRegister

const styles = StyleSheet.create({

  customInputOTP: {
    height: 60,
    width: 60,
    fontSize: 20,
    color: '#000',
    fontWeight: '600',
    textAlign: 'center',
    borderRadius: 50,
    marginHorizontal: 8,
    backgroundColor: '#FFF3E9',
  },
})