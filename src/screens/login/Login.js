import React, { useState } from 'react'
import { StyleSheet, Text, Pressable, View, Image, Alert } from 'react-native'
import { TextInputFloating, Button, KeyboardView } from "../../components";
import NavigationService from "../../tools/navigationService";
import { NAV_NAME_OTP_LOGIN, NAV_NAME_REGISTER } from '../../tools/constant';
import { android, iconTools } from "../../tools/helper";
import { SafeAreaProvider } from 'react-native-safe-area-context';

export default function App() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [iconPasssword, setIconPassword] = useState('eye')
    const [showPassword, setShowPassword] = useState(true)

    const handlerVisiblePassword = () => {
        setShowPassword(!showPassword)
        if (!showPassword) return setIconPassword('eye')
        if (showPassword) return setIconPassword('eye-off')
    }
    return (
        <KeyboardView contentContainerStyle={{ flex: android ? null : 1 }}>
            <SafeAreaProvider style={{ backgroundColor: '#23286B' }}>
                <Text style={styles.title}>POINS</Text>
                <View style={styles.content}>
                    <View>
                        <TextInputFloating
                            style={[{ marginBottom: 30 }]}
                            iconType={iconTools.MaterialCommunityIcons}
                            IconName={'email'}
                            iconSize={24}
                            iconColor={'#23286B'}
                            iconActive={true}
                            value={email}
                            label="Email"
                            onChangeText={(text) => setEmail(text)}
                        />
                        <TextInputFloating
                            secureTextEntry={showPassword}
                            style={[{ marginBottom: 10 }]}
                            iconType={iconTools.MaterialCommunityIcons}
                            IconName={iconPasssword}
                            iconSize={24}
                            iconColor={'#23286B'}
                            iconActive={true}
                            value={password}
                            label="Password"
                            onPressed={() => handlerVisiblePassword()}
                            onChangeText={(text) => setPassword(text)}
                        />
                    </View>
                    <Pressable
                        onPress={() => Alert.alert('NOTHING')}
                        style={{ alignSelf: 'flex-end' }}
                    >
                        <Text style={{ color: '#23286B', fontWeight: '600', fontSize: 12, marginBottom: 70 }}>Forgot Password?</Text>
                    </Pressable>
                    <Button caption='Login' onPress={() => NavigationService.navigate(NAV_NAME_OTP_LOGIN)} />
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: android ? 20 : 50 }}>
                        <Text style={{ fontSize: 12, color: '#000' }} >Don't have an account? </Text>
                        <Pressable
                            style={{}}
                            onPress={() => NavigationService.navigate(NAV_NAME_REGISTER)}
                        >
                            <Text style={{ color: '#F9603B', fontWeight: '500' }}>Sign Up</Text>
                        </Pressable>
                    </View>
                    <Image
                        source={require('../../assets/meeting.png')}
                        style={{ height: 200, width: '100%', alignSelf: 'center', }}
                    />
                </View>
            </SafeAreaProvider>
        </KeyboardView>
    )
}

const styles = StyleSheet.create({
    content: {
        height: '100%',
        backgroundColor: 'white',
        paddingHorizontal: 20,
        borderTopStartRadius: 30,
        borderTopEndRadius: 30,
        paddingTop: 60,
    },
    title: {
        color: 'white',
        fontSize: 36,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingVertical: '20%'
    },
})