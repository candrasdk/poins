export { default as HomeScreen } from './home';
export { default as LoginScreen } from './login';
export { default as RegisterScreen } from './register';
export { default as OtpRegisterScreen } from './otpRegister';
export { default as OtpLoginScreen } from './otpLogin';
export { default as BookingScreen } from './booking';

