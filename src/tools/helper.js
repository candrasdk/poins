import { Dimensions, Platform } from "react-native";
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';


export const iconTools = {
  MaterialCommunityIcons,
  MaterialIcons,
  Ionicons,
  Feather,
  AntDesign,
  Entypo,
  Octicons,
  EvilIcons,
}
export const ios = Platform.OS === 'ios';
export const android = Platform.OS === 'android';

export const getScreenDimension = () => {
    const { height, width } = Dimensions.get('window');
    return { height, width };
};

export const sortAsc = (a, b) => (a > b ? 1 : -1);
export const sortDesc = (a, b) => (a > b ? -1 : 1);

export const convertArrToObj = (array, objKey) => (!array ? {} : array.reduce(
  (obj, item) => ({ ...obj, [item[objKey]]: item }), {},
));

