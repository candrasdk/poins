export const NAV_NAME_SPLASH = 'NAV_NAME_SPLASH';
export const NAV_NAME_HOME = 'NAV_NAME_HOME';
export const NAV_NAME_LOGIN = 'NAV_NAME_LOGIN';
export const NAV_NAME_REGISTER = 'NAV_NAME_REGISTER';
export const NAV_NAME_OTP_REGISTER = 'NAV_NAME_OTP_REGISTER';
export const NAV_NAME_OTP_LOGIN = 'NAV_NAME_OTP_LOGIN';
export const NAV_NAME_BOOKING = 'NAV_NAME_BOOKING';


export const COLOR_WHITE = '#FFFFFF';
export const COLOR_BLACK = '#000000';
export const COLOR_DISABLED = '#979797';
export const COLOR_TRANSPARENT_DARK = 'rgba(0, 0, 0, 0.1)';
export const COLOR_HORIZONTAL_LINE = '#E5E5E5';

export const FONT_NUNITO_REGULAR = 'Nunito-Regular';
export const FONT_NUNITO_BOLD = 'Nunito-Bold';
export const FONT_NUNITO_SEMI_BOLD = 'Nunito-SemiBold';
export const FONT_NUNITO_EXTRA_BOLD = 'Nunito-ExtraBold';
