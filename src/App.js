import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import {
  HomeScreen, LoginScreen, RegisterScreen,OtpRegisterScreen, OtpLoginScreen,
  BookingScreen
} from "./screens";
import {
   NAV_NAME_HOME, NAV_NAME_LOGIN, NAV_NAME_REGISTER, NAV_NAME_OTP_REGISTER,
  NAV_NAME_OTP_LOGIN, NAV_NAME_BOOKING
} from "./tools/constant";
import { NavigationContainer, } from '@react-navigation/native';
import NavigationService from './tools/navigationService';
import { StatusBar } from 'react-native';


const Stack = createStackNavigator();

function MainNavigation() {
  return (
    <NavigationContainer ref={NavigationService.navigationRef} >
        <StatusBar translucent backgroundColor="transparent" />
      <Stack.Navigator>
        <Stack.Screen
          name={NAV_NAME_HOME}
          component={HomeScreen}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forScaleFromCenterAndroid,
            headerShown: false,
          }}
        />
        <Stack.Screen
          name={NAV_NAME_LOGIN}
          component={LoginScreen}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forFadeFromCenter,
            headerShown: false,
          }}
        />
        <Stack.Screen
          name={NAV_NAME_REGISTER}
          component={RegisterScreen}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forFadeFromCenter,
            headerShown: false,
          }}
        />
        <Stack.Screen
          name={NAV_NAME_OTP_REGISTER}
          component={OtpRegisterScreen}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            headerShown: false,
          }}
        />
        <Stack.Screen
          name={NAV_NAME_OTP_LOGIN}
          component={OtpLoginScreen}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forVerticalIOS,
            headerShown: false,
          }}
        />
        <Stack.Screen
          name={NAV_NAME_BOOKING}
          component={BookingScreen}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forVerticalIOS,
            headerShown: false,
          }}
        />
      </Stack.Navigator>
      
    </NavigationContainer>
  );
}

export default MainNavigation;